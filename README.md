# Pipeline Buster

This is a short game developed as a teaching aid to help students understand processor pipelines and pipeline hazards.
It feature three short levels where the player must attack the simulated machine by breaking its pipeline execution via
the execution of assembly code.

It features an (embarrassingly) rudimentary assembly interpreter written in JavaScript, and the game itself is written
in react, with a python flask server behind it.

I enjoyed the result, but please do not look too closely at the code. It is awful and not representative of my standards :)

Ah, also, it is in Portuguese.


![](doc/fibo.gif) 
A short clip

## Installing and running

Install both the Javascript and Python dependencies:
```shell script
pip install -r requirements.txt
npm install
```

And run
```shell script
python pbust.py &
npm run dev
```

The game should be on **localhost:1337**

## The assembly on the game

This is it's EBNF-ish grammar:

```ebnf
line ::= label? operation argumentlist? comment? | comment
label ::= [A-Z]+ ':' space
space ::= ' '+
operation ::= [A-Z]+ space
argumentlist ::= argument | argument space argument
argument ::= number | register | address
number ::= [0-9]+
register ::= [A-Z]+
address ::= '['number']' | '['register']'
comment ::= ';' .+
```

And here an example fibonacci program

```assembly
;--------------------;
; Example: FIBONACCI ;
;--------------------;
PUSH 1 ; Initial values
PUSH 1
MOV C 10 ; Loop 0..10
LOOP: DEC C
JZ END
POP A
POP B
PUSH B
PUSH A
ADD A B ; nth value
; move to memory, just to show it off
MOV [C] A
MOV A [C]
PUSH A
JMP LOOP
END: PRS ; print sequence
MOV A 0
HLT
```

I might redo the whole thing with nicer parsing... Maybe do the interpreter on the backend. Maybe use [this](https://sap.github.io/chevrotain)